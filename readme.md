### Install
Git Clone the repository

    git clone https://gitlab.com/AwesomeZebra/gymwolf_selenide_tests

### Prerequisites
- Apache Maven 3.6.0
- Java 1.8

### Setup
- src/test/java/conf/AppData - application settings
- src/test/java/conf/SelenideConfiguration - browser settings
- download chromedriver or geckodriver corresponding your browser version:
    - ChromeDriver(http://chromedriver.chromium.org/)
    - GeckoDriver(https://github.com/mozilla/geckodriver/releases)
- Move the driver executable to /drivers folder in the project tree. Make sure you change the driver paths in AppData or supply the correct paths with environment variables.



### Run
- Command-line: mvn test (you can supply environment variables here with -DVARIABLE_NAME=VARIABLE_VALUE, e.g 

        mvn test -DCHROMEDRIVER_LOCATION=/home/downloads/gymwolf_selenide_tests/drivers/chromedriver_63
- IntelliJ Idea - right click on TestGymWorkoutAddViewDelete.java -> Run 'TestGymWorkoutAddViewDelete'