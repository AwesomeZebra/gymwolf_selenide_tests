package pageobjects;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

public class BrowseWorkoutsPage {

    private static final String WORKOUT_SELECTOR_BY_ID_START_XPATH = "//a[@data-id=";
    private static final String WORKOUT_YEAR_HEADING_XPATH =
            "/html/body/div[contains(@class, \"container\")]/ul/li[contains(@class, \"center\")]/h3";
    private static final String WORKOUT_YEAR_SELECT_PREVIOUS =
            "/html/body/div[contains(@class, \"container\")]/ul/li[contains(@class, \"previous\")]/a";
    private static final String WORKOUT_YEAR_SELECT_NEXT =
            "/html/body/div[contains(@class, \"container\")]/ul/li[contains(@class, \"next\")]/a";


    // Workflow methods (always return a pageobject
    public BrowseWorkoutsPage navigateToYear(int desiredYear) {
        int browserYear = getBrowserYear();

        SelenideElement button;
        if (browserYear != desiredYear) {
            if (browserYear < desiredYear) {
                button = $(By.xpath(WORKOUT_YEAR_SELECT_NEXT));
            }
            else {
                button = $(By.xpath(WORKOUT_YEAR_SELECT_PREVIOUS));
            }
            for (int i = 0; i < Math.abs(browserYear - desiredYear); i++) {
                button.click();
            }
        }
        return page(BrowseWorkoutsPage.class);
    }


    // Get methods (don't return pageobject)
    public boolean getWorkoutExistsById(String id) {
        return $(By.xpath(WORKOUT_SELECTOR_BY_ID_START_XPATH + id + "]")).exists();
    }

    // Helper methods
    private int getBrowserYear() {
        String title = $(By.xpath(WORKOUT_YEAR_HEADING_XPATH)).getText();
        String yearPartOfTitle = title.substring(0, title.indexOf(",")).replaceAll("\\D+","");
        return Integer.parseInt(yearPartOfTitle);
    }


}
