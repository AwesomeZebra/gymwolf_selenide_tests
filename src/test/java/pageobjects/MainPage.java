package pageobjects;

import com.codeborne.selenide.Condition;
import conf.Constants;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

public class MainPage {

    private static final String NEW_WORKOUT_BUTTON_NO_WORKOUTS_CSS =
            "body > div.stats-row > div > div > div > p > a.btn-success";
    private static final String NEW_WORKOUT_BUTTON_WORKOUTS_EXIST_NXS_CSS =
            "body > div.stats-row > div > div > div > p.hidden-xs > a.btn-success";
    private static final String NEW_WORKOUT_BUTTON_WORKOUTS_EXIST_XS_CSS =
            "body > div.stats-row > div > div > div > p.visible-xs > a.btn-success";
    private static final String WORKOUTS_LIST_TABLE_XPATH =
            "/html/body/div[contains(@class, \"stats-row\")]/div/div/div/table";
    private static final String WORKOUT_LIST_WORKOUT_LINK_XPATH =
            "//div[contains(@class, \"stats-row\")]/div/div/div/table/tbody/tr/td/a[contains(@href, \"";
    private static final String BROWSE_ALL_WORKOUTS_BUTTON =
            "/html/body/div[contains(@class, \"stats-row\")]/div/div/div/table/tbody/tr/td/" +
                    "a[contains(@class, \"btn-link\")]";

    public WorkoutPage addNewWorkout() {

        // New workout button location depends on if there are any workouts already for the user.
        // Check if workouts table exists and click the correct element accordingly
        if ($(By.xpath(WORKOUTS_LIST_TABLE_XPATH)).is(Condition.exist)) {
            int pageWidth = $("body").getSize().getWidth();
            if (pageWidth < Constants.BOOTSTRAPXSWIDTHPX) {
                // Page is in xs mode
                $(NEW_WORKOUT_BUTTON_WORKOUTS_EXIST_XS_CSS).click();
            }
            else {
                // Page is in normal width mode
                $(NEW_WORKOUT_BUTTON_WORKOUTS_EXIST_NXS_CSS).click();
            }
        }
        else {
            $(NEW_WORKOUT_BUTTON_NO_WORKOUTS_CSS).click();
        }
        return page(WorkoutPage.class);
    }

    public WorkoutPage openExistingWorkoutByID(String id) {
        $(By.xpath(WORKOUT_LIST_WORKOUT_LINK_XPATH + id + "\")]")).click();
        return page(WorkoutPage.class);
    }

    public BrowseWorkoutsPage openWorkoutList() {
        $(By.xpath(BROWSE_ALL_WORKOUTS_BUTTON)).click();
        return page(BrowseWorkoutsPage.class);
    }
}
