package pageobjects;

import conf.SelenideConfiguration;
import conf.AppData;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.url;


public class HomePage {

    private static final String SIGN_IN_XPATH = "//div[@id=\"main-menu\"]/ul/li/a[@title=\"Sign In\"]";
    private static final String EMAIL_FIELD_XPATH =
            "//div[@id=\"login-front\"]/div/div/div/div/div/form/div/input[@name=\"email\"]";
    private static final String PASSWORD_FIELD_XPATH =
            "//div[@id=\"login-front\"]/div/div/div/div/div/form/div/input[@name=\"password\"]";
    private static final String LOGIN_BUTTON_XPATH =
            "//*[@id=\"login-front\"]/div/div/div/div/div/form/div/div/div/button";
    private static final String NAVBAR_NAVIGATE_TO_HOME = "//a[@title=\"Gymwolf\"]";



    public static MainPage loginToGymWolf(String email, String password) {
        // First check if browser is already open
        try {
            url();
            // Url exists - browser is open -> check if we are logged in
            if ($(By.xpath(SIGN_IN_XPATH)).exists()) {
                return loginWithUsernameAndPassword(email, password);
            }
            else {
                // Navigate to main page
                $(By.xpath(NAVBAR_NAVIGATE_TO_HOME)).click();
                return page(MainPage.class);
            }
        }
        catch (IllegalStateException e) {
            // Browser is not open
            SelenideConfiguration.configure();
            // Open the webpage
            open(AppData.URL);
            return loginWithUsernameAndPassword(email, password);
        }
    }

    // Helper/extension methods
    private static MainPage loginWithUsernameAndPassword(String email, String userPassword) {


        // Click "Sign in"
        $(By.xpath(SIGN_IN_XPATH)).click();

        // Fill out email field
        $(By.xpath(EMAIL_FIELD_XPATH)).setValue(email);

        // Fill out password field
        $(By.xpath(PASSWORD_FIELD_XPATH)).setValue(userPassword);

        // Click "Sign in" button
        $(By.xpath(LOGIN_BUTTON_XPATH)).click();
        return page(MainPage.class);
    }

}