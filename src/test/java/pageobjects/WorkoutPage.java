package pageobjects;

import objects.GymWorkoutExercise;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import conf.AppData;
import org.openqa.selenium.*;
import utils.DateUtils;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.url;

public class WorkoutPage {

    // General selectors
    private static final String GYM_WORKOUT_TAB_XPATH = "//a[@data-form=\".workout-mode-gym\"]";
    private static final String CARDIO_WORKOUT_TAB_XPATH = "//a[@data-form=\".workout-mode-cardio\"]";
    private static final String ALL_EXERCISE_ROWS_XPATH = "//div[@class=\"workout-data\"]/div[@class=\"row data-row\"]";

    // Workout form specific selectors
    private static final String REMOVE_BUTTON_CSS = ".delete_exercise";
    private static final String EXERCISE_ROW_FIELDS_START_XPATH =
            "//*[@id=\"workout_form\"]/div[contains(@class, \"workout-data\")]/div[contains(@class, \"data-row\")][";
    private static final String EXERCISE_NAME_INPUT_FIELD_END_XPATH =
            "]/div[contains(@class, \"exercise_column\")]/div/input[@name=\"exercise_name[]\"]";
    private static final String EXERCISE_ROW_REPS_FIELDS_MID_XPATH = "]/div/div[";
    private static final String EXERCISE_ROW_KG_INPUT_FIELD_END_XPATH =
            "]/input[contains(@class, \"exercise_weight\")]";
    private static final String EXERCISE_ROW_REPS_INPUT_FIELD_END_XPATH =
            "]/input[contains(@class, \"exercise_reps\")]";
    private static final String VISIBLE_EXERCISE_DROPDOWN_ELEMENTS_XPATH =
            "//ul[contains(@class, \"ui-autocomplete\")][contains(@style, \"display: block;\")]/li/a";
    private static final String BODYWEIGHT_INPUT_ID = "bodyweight";
    private static final String NOTES_INPUT_ID = "notes";
    private static final String SAVE_WORKOUT_BUTTON_XPATH =
            "//*[@id=\"workout_form\"]/div[contains(@class, \"form-actions\")]/div/div/" +
                    "button[contains(@class, \"btn-success\")]";
    private static final String DELETE_WORKOUT_BUTTON_XPATH =
            "//*[@id=\"workout_form\"]/div[contains(@class, \"form-actions\")]/div/div/a[contains(@class, " +
                    "\"btn-danger\")][@data-form=\".delete-workout\"]";

    // Datepicker selectors
    private static final String DATEPICKER_FIELD_ID = "workout_display_date";
    private static final String DATEPICKER_YEAR_XPATH = "//span[@class=\"ui-datepicker-year\"]";
    private static final String DATEPICKER_MONTH_XPATH = "//span[@class=\"ui-datepicker-month\"]";
    private static final String DATEPICKER_PREVIOUS_MONTH_BUTTON_XPATH =
            "//a[contains(@class, \"ui-datepicker-prev\")]";
    private static final String DATEPICKER_NEXT_MONTH_BUTTON_XPATH = "//a[contains(@class, \"ui-datepicker-next\")]";
    private static final String DATEPICKER_DATE_CELL_XPATH = "//a[text()=\"";

    //
    private static final String WORKOUT_SAVED_ALERT_XPATH =
            "//body/div/div[contains(@class, \"alert-container\")]/div[contains(@class, \"alert-success\")]";
    private static final String WORKOUT_EXERCISE_ROW_REPS_XPATH =
            "//div[contains(@class, \"data-row\")]/div/span[contains(@class, \"reps\")]";
    private static final String WORKOUT_EXERCISE_ROW_KGS_XPATH =
            "//div[contains(@class, \"data-row\")]/div/span[contains(@class, \"total_weight\")]";
    private static final String WORKOUT_TOTAL_REPS_XPATH =
            "//div[contains(@class, \"workout-totals\")]/div/span[contains(@class, \"reps_sum\")]";
    private static final String WORKOUT_TOTAL_KGS_XPATH =
            "//div[contains(@class, \"workout-totals\")]/div/span[contains(@class, \"total_weight_sum\")]";

    public WorkoutPage selectGymWorkout() {
        $(By.xpath(GYM_WORKOUT_TAB_XPATH)).click();
        return page(WorkoutPage.class);
    }

    public WorkoutPage selectCardioWorkout() {
        $(By.xpath(CARDIO_WORKOUT_TAB_XPATH)).click();
        return page(WorkoutPage.class);
    }

    public WorkoutPage selectWorkoutDate(LocalDate workoutDate) {
        $(By.id(DATEPICKER_FIELD_ID)).click();
        // Select correct year
        selectCorrectCalendarMonth(workoutDate);
        selectDateFromCalendar(workoutDate.getDayOfMonth());
        return page(WorkoutPage.class);
    }

    // dropdownTestLetterCount - to test selecting from dropdown, value is as many letters should be written in the
    // dropdown before the correct value is clicked in the list. 0 = no test, the whole value is written in the field.
    // testAutoComplete - if this value is true, exercise name will be written in the field until just one value remains
    // in the dropdown - after which focus will be lost and the field should be auto-populated
    public WorkoutPage addNewExerciseRow(GymWorkoutExercise exercise, int dropdownTestLetterCount,
                                         boolean testAutoComplete) {
        // Check that kg array and reps array have same number of elements
        if (exercise.kgs.length == exercise.reps.length) {
            // Find the next empty row. We know it's an empty row, if it doesn't have the remove button visible"
            // 1. Find all exercise rows
            ElementsCollection exerciseRows = $$(By.xpath(ALL_EXERCISE_ROWS_XPATH));
            // 2. Loop through each row and check if this row has the row delete button visible
            for (int rowNum = 1; rowNum <= exerciseRows.size(); rowNum++) {
                if (!exerciseRows.get(rowNum - 1).find(REMOVE_BUTTON_CSS).isDisplayed()) {
                    // This is the row without exercise inserted
                    // 3. Fill out the form fields
                    // We pass rowNum instead of the row because of weird Selenide reference issues
                    fillOutGymWorkoutExerciseName(rowNum, exercise.name, dropdownTestLetterCount, testAutoComplete);
                    // Add all the reps sets
                    for (int repSet = 1; repSet <= exercise.kgs.length; repSet++) {
                        fillOutGymWorkoutKilograms(rowNum, repSet, exercise.kgs[repSet - 1]);
                        fillOutGymWorkoutReps(rowNum, repSet, exercise.reps[repSet - 1]);
                    }

                    return page(WorkoutPage.class);
                }
            }
        }
        else {
            throw new InvalidArgumentException("kg array length (" + exercise.kgs.length +
                    ") does not match reps array length (" + exercise.reps.length + ")");
        }
        return page(WorkoutPage.class);

    }

    public WorkoutPage insertBodyWeight(float bodyWeight) {
        $(By.id(BODYWEIGHT_INPUT_ID)).setValue(String.valueOf(bodyWeight));
        return page(WorkoutPage.class);
    }

    public WorkoutPage insertNotes(String notes) {
        $(By.id(NOTES_INPUT_ID)).setValue(String.valueOf(notes));
        return page(WorkoutPage.class);
    }

    public WorkoutPage saveWorkoutWithButtonClick() {
        $(By.xpath(SAVE_WORKOUT_BUTTON_XPATH)).click();
        return page(WorkoutPage.class);
    }

    public MainPage deleteWorkout() {
        $(By.xpath(DELETE_WORKOUT_BUTTON_XPATH)).click();
        confirm();
        return page(MainPage.class);
    }

    // Get methods - these don't return pageobject, but some values, so these can't be used to chain workflows
    public boolean getSavedAlertExists() {
        return $(By.xpath(WORKOUT_SAVED_ALERT_XPATH)).exists();
    }

    public int[] getExerciseReps() {
        ElementsCollection repsElements = $$(By.xpath(WORKOUT_EXERCISE_ROW_REPS_XPATH));
        int[] reps = new int[repsElements.size()];

        for (int repNum = 0; repNum < repsElements.size(); repNum++) {
            String repAmountString = repsElements.get(repNum).getText();
            if (!repAmountString.equals("")) {
                reps[repNum] = Integer.parseInt(repAmountString);
                }
            else {
                reps[repNum] = 0;
            }
        }
        return reps;
    }

    public float[] getExerciseKgs() {
        ElementsCollection kgsElements = $$(By.xpath(WORKOUT_EXERCISE_ROW_KGS_XPATH));
        float[] kgs = new float[kgsElements.size()];

        for (int kgNum = 0; kgNum < kgsElements.size(); kgNum++) {
            String repAmountString = kgsElements.get(kgNum).getText();
            if (!repAmountString.equals("")) {
                kgs[kgNum] = Float.parseFloat(repAmountString);
            }
            else {
                kgs[kgNum] = 0f;
            }
        }
        return kgs;
    }

    public int getWorkoutReps() {
        return Integer.parseInt($(By.xpath(WORKOUT_TOTAL_REPS_XPATH)).getText());
    }

    public float getWorkoutKgs() {
        return Float.parseFloat($(By.xpath(WORKOUT_TOTAL_KGS_XPATH)).getText());
    }

    public float getBodyWeight() {
        return Float.parseFloat($(By.id(BODYWEIGHT_INPUT_ID)).getValue());
    }

    public String getNotes() {
        return $(By.id(NOTES_INPUT_ID)).getText();
    }

    public String getWorkoutID() {
        return url().replaceAll("\\D+","");
    }

    // Private helper methods
    private void selectCorrectCalendarMonth(LocalDate workoutDate) {
        // Find out how many clicks do we have to make to navigate to the correct calendar page
        LocalDate calendarDate = LocalDate.of(
                Integer.parseInt($(By.xpath(DATEPICKER_YEAR_XPATH)).getText()),
                DateUtils.getMonthIntegerFromString($(By.xpath(DATEPICKER_MONTH_XPATH)).getText()),
                workoutDate.getDayOfMonth());
        long monthsBetween = ChronoUnit.MONTHS.between(calendarDate, workoutDate);

        // Find out which button to click
        SelenideElement button;
        if (monthsBetween < 0) {
            button = $(By.xpath(DATEPICKER_PREVIOUS_MONTH_BUTTON_XPATH));
        }
        else {
            button = $(By.xpath(DATEPICKER_NEXT_MONTH_BUTTON_XPATH));
        }

        // Click the button
        for (int i = 0; i < Math.abs(monthsBetween); i++) {
            button.click();
        }

        // TODO: Add checks that correct month is selected
    }


    private void selectDateFromCalendar(int dayOfMonth) {
        $(By.xpath(DATEPICKER_DATE_CELL_XPATH + dayOfMonth + "\"]")).click();
    }

    private void fillOutGymWorkoutReps(int rowNum, int repSet, int reps) {
        $(By.xpath(EXERCISE_ROW_FIELDS_START_XPATH + rowNum + EXERCISE_ROW_REPS_FIELDS_MID_XPATH + repSet
                + EXERCISE_ROW_REPS_INPUT_FIELD_END_XPATH))
                .setValue(String.valueOf(reps));
    }

    private void fillOutGymWorkoutKilograms(int rowNum, int repSet, float kg) {
        $(By.xpath(EXERCISE_ROW_FIELDS_START_XPATH + rowNum + EXERCISE_ROW_REPS_FIELDS_MID_XPATH + repSet
                + EXERCISE_ROW_KG_INPUT_FIELD_END_XPATH))
                .setValue(String.valueOf(kg));
    }

    private void fillOutGymWorkoutExerciseName(int rowNum, String exercise, int dropdownTestLetterCount,
                                               boolean testAutoComplete) {
        if (testAutoComplete) {
            // Write in the exercise name letter by letter until only one value remains,
            // after which navigate away from the field and the field should be auto-populated
            // TODO: Implement for future test cases

        }
        else {

            try {
                SelenideElement exerciseField = $(By.xpath(EXERCISE_ROW_FIELDS_START_XPATH +
                        rowNum + EXERCISE_NAME_INPUT_FIELD_END_XPATH));
                // Field exists
                // If the dropdownTestLetterCount == 0, just insert the whole exercise name to the field
                // and click elsewhere on the page to unfocus the field
                if (dropdownTestLetterCount == 0) {
                    exerciseField.setValue(exercise);
                    // Since autocomplete won't work until dropdown has appeared, we have to wait for it to be displayed
                    $(By.xpath(VISIBLE_EXERCISE_DROPDOWN_ELEMENTS_XPATH)).waitUntil(Condition.visible,
                            AppData.SELENIDE_TIMEOUT);
                    $("body").click();
                }
                else {
                    exerciseField.setValue(exercise.substring(0, dropdownTestLetterCount));
                    // Wait for the dropdown to open
                    $(By.xpath(VISIBLE_EXERCISE_DROPDOWN_ELEMENTS_XPATH)).waitUntil(Condition.visible,
                            AppData.SELENIDE_TIMEOUT);
                    // And get the dropdown element list
                    ElementsCollection exerciseValues = $$(By.xpath(VISIBLE_EXERCISE_DROPDOWN_ELEMENTS_XPATH));
                    for (SelenideElement exerciseValue : exerciseValues) {
                        // If dropdown row value is exact match to exercise, click on it.
                        if (exerciseValue.getText().equals(exercise)) {
                            exerciseValue.click();
                            return;
                        }
                    }
                    // Whole loop didn't yield any exact matches. Throw error.
                    throw new InvalidArgumentException("Exercise value (" + exercise + ") was not found in dropdown.");
                }
            }
            catch (NoSuchElementException e) {
                // If there's a problem with this element being loaded in f.e server side, implement a retry
                throw e;
            }
        }
    }
}
