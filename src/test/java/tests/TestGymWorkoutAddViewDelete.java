package tests;

import objects.GymWorkout;
import objects.GymWorkoutExercise;
import conf.AppData;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestMethodOrder;
import pageobjects.BrowseWorkoutsPage;
import pageobjects.HomePage;
import org.junit.jupiter.api.Test;
import pageobjects.WorkoutPage;
import utils.DateUtils;


import java.time.LocalDate;

import org.junit.jupiter.api.Order;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TestGymWorkoutAddViewDelete {

    /* Test data for all sub-tests */
    private GymWorkout gymWorkout = new GymWorkout(new GymWorkoutExercise[]{new GymWorkoutExercise("Push Press", new float[]{12.5f, 15f}, new int[]{8, 10}), new GymWorkoutExercise("Arnold Press", new float[]{33f}, new int[]{5})}, 82f, "LegdayFTW");
    private LocalDate workoutDate = DateUtils.getYesterdayLocalDate();

    /* Global variables to hold data between sub-tests */
    private static String workoutID;


    @Test
    @Order(1)
    // Create a new gym workout
    void testAddNewGymWorkout() {
        /* Test data (test specific) */

        /* Test execution */
        WorkoutPage savedWorkout = HomePage.loginToGymWolf(AppData.USER_EMAIL, AppData.USER_PASSWORD)
                .addNewWorkout()
                .selectGymWorkout()
                .selectWorkoutDate(workoutDate)
                .addNewExerciseRow(gymWorkout.exercises[0], 0, false)
                .addNewExerciseRow(gymWorkout.exercises[1], 3, false)
                .insertBodyWeight(gymWorkout.bodyweight)
                .insertNotes(gymWorkout.notes)
                .saveWorkoutWithButtonClick();

        /* Save global test data */
        workoutID = savedWorkout.getWorkoutID();

        /* Test assertions */
        // Assert that workout was saved
        assertTrue(savedWorkout.getSavedAlertExists());
        // Assert exercise 1 reps and weight
        assertEquals(gymWorkout.exercises[0].getTotalReps(), savedWorkout.getExerciseReps()[0]);
        assertEquals(gymWorkout.exercises[0].getTotalKgs(), savedWorkout.getExerciseKgs()[0]);
        // Assert exercise 2 reps and weight
        assertEquals(gymWorkout.exercises[1].getTotalReps(), savedWorkout.getExerciseReps()[1]);
        assertEquals(gymWorkout.exercises[1].getTotalKgs(), savedWorkout.getExerciseKgs()[1]);

        // Assert total workout reps and weight
        assertEquals(gymWorkout.getTotalReps(), savedWorkout.getWorkoutReps());
        assertEquals(gymWorkout.getTotalKgs(), savedWorkout.getWorkoutKgs());
    }

    @Test
    @Order(2)
    // Open the previously made workout
    void testOpenGymWorkout() {
        /* Test data (test specific) */

        /* Test execution */
        WorkoutPage openedWorkout = HomePage
                .loginToGymWolf(AppData.USER_EMAIL, AppData.USER_PASSWORD)
                .openExistingWorkoutByID(workoutID);

        /* Test assertions */
        // Assert that previously set values are still the same
        // Assert exercise 1 reps and weight
        assertEquals(gymWorkout.exercises[0].getTotalReps(), openedWorkout.getExerciseReps()[0]);
        assertEquals(gymWorkout.exercises[0].getTotalKgs(), openedWorkout.getExerciseKgs()[0]);
        // Assert exercise 2 reps and weight
        assertEquals(gymWorkout.exercises[1].getTotalReps(), openedWorkout.getExerciseReps()[1]);
        assertEquals(gymWorkout.exercises[1].getTotalKgs(), openedWorkout.getExerciseKgs()[1]);

        // Assert total workout reps and weight
        assertEquals(gymWorkout.getTotalReps(), openedWorkout.getWorkoutReps());
        assertEquals(gymWorkout.getTotalKgs(), openedWorkout.getWorkoutKgs());

        // Assert bodyweight
        assertEquals(gymWorkout.bodyweight, openedWorkout.getBodyWeight());

        // Assert notes
        assertEquals(gymWorkout.notes, openedWorkout.getNotes());
    }

    @Test
    @Order(3)
    // Open the previously made workout and delete it
    void testDeleteGymWorkout() {
        /* Test data (test specific) */

        /* Test execution */
        BrowseWorkoutsPage browseWorkoutsPage = HomePage.loginToGymWolf(AppData.USER_EMAIL, AppData.USER_PASSWORD)
                .openExistingWorkoutByID(workoutID)
                .deleteWorkout()
                .openWorkoutList()
                .navigateToYear(workoutDate.getYear());

        /* Test assertions */
        // Assert that workorder with the ID does not exist anymore
        assertFalse(browseWorkoutsPage.getWorkoutExistsById(workoutID));
    }
}
