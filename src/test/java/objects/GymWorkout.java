package objects;

public class GymWorkout {
    public GymWorkoutExercise[] exercises;
    public float bodyweight;
    public String notes;

    public GymWorkout(GymWorkoutExercise[] exercises, float bodyweight, String notes) {
        this.exercises = exercises;
        this.bodyweight = bodyweight;
        this.notes = notes;
    }

    public GymWorkout(GymWorkoutExercise[] exercises, float bodyweight) {
        this.exercises = exercises;
        this.bodyweight = bodyweight;
    }

    public GymWorkout(GymWorkoutExercise[] exercises, String notes) {
        this.exercises = exercises;
        this.notes = notes;
    }

    public GymWorkout(GymWorkoutExercise[] exercises) {
        this.exercises = exercises;
    }

    public int getTotalReps() {
        int totalReps = 0;
        for (GymWorkoutExercise ex : exercises) {
            totalReps += ex.getTotalReps();
        }
        return totalReps;
    }

    public float getTotalKgs() {
        float totalKgs = 0f;
        for (GymWorkoutExercise ex : exercises) {
            totalKgs += ex.getTotalKgs();
        }
        return totalKgs;
    }
}
