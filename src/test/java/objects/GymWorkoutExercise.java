package objects;

import java.util.stream.IntStream;

public class GymWorkoutExercise {
    public String name;
    public float[] kgs;
    public int[] reps;

    public GymWorkoutExercise(String name, float[] kgs, int[] reps) {
        this.name = name;
        this.kgs = kgs;
        this.reps = reps;
    }

    public int getTotalReps() {
        return IntStream.of(reps).sum();
    }

    public float getTotalKgs() {
        float totalKgs = 0f;
        for (int i = 0; i < kgs.length; i++) {
            totalKgs += kgs[i] * reps[i];
        }
        return totalKgs;
    }
}
