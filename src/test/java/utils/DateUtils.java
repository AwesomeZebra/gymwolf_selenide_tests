package utils;

import java.time.LocalDate;

public class DateUtils {
    public static int getMonthIntegerFromString(String monthName) {
        switch (monthName) {
            case "Jaanuar":
            case "January":
                return 1;
            case "Veebruar":
            case "February":
                return 2;
            case "Märts":
            case "March":
                return 3;
            case "Aprill":
            case "April":
                return 4;
            case "Mai":
            case "May":
                return 5;
            case "Juuni":
            case "June":
                return 6;
            case "Juuli":
            case "July":
                return 7;
            case "August":
                return 8;
            case "September":
                return 9;
            case "Oktoober":
            case "October":
                return 10;
            case "November":
                return 11;
            case "Detsember":
            case "December":
                return 12;
        };
        // Default to January
        return 1;
    }

    public static LocalDate getYesterdayLocalDate() {
        return LocalDate.now().minusDays(1);
    }

    public static LocalDate getTomorrowLocalDate() {
        return LocalDate.now().plusDays(1);
    }

    public static LocalDate getLocalDateInPastOrFutureByDaysOffset(int daysOffset) {
        return LocalDate.now().plusDays(daysOffset);
    }
}
