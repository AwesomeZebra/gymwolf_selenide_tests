package conf;

import com.codeborne.selenide.Configuration;
import static conf.AppData.SELENIDE_TIMEOUT;

public class SelenideConfiguration {
    // https://selenide.org/javadoc/current/index.html?com/codeborne/selenide/Configuration.html
    public static void configure() {

        if (System.getProperty("SELENIDE_BROWSER", "").equals("firefox")) {
            // Geckodriver location
            System.setProperty("webdriver.gecko.driver", System.getProperty("GECKODRIVER_LOCATION", AppData.GECKODRIVER_LOCATION));
        }
        // default to Chrome
        else {
            // Chromedriver location
            System.setProperty("webdriver.chrome.driver", System.getProperty("CHROMEDRIVER_LOCATION", AppData.CHROMEDRIVER_LOCATION));
        }

        Configuration.browser = System.getProperty("SELENIDE_BROWSER", "chrome");
        Configuration.headless = System.getProperty("SELENIDE_HEADLESS", "false").equals("true");
        Configuration.startMaximized = true;
        Configuration.timeout = SELENIDE_TIMEOUT;

        Configuration.holdBrowserOpen = true;
    }
}
